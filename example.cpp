// This program plays the most famous tetris melody in a loop
// To compile run `g++ ./example.cpp -lSDL2`
#include "tonegen.hpp"

#include <iostream>
#include <chrono>
#include <thread>



int main(int argc, char* argv[]) {
    ToneGen gen;

    std::initializer_list<std::tuple<ToneGen::Note, uint>> melody = {
      {ToneGen::Note::E5, 4}, {ToneGen::Note::B4, 8}, {ToneGen::Note::C5, 8}, {ToneGen::Note::D5, 4}, {ToneGen::Note::C5, 8}, {ToneGen::Note::B4, 8},
      {ToneGen::Note::A4, 4}, {ToneGen::Note::A4, 8}, {ToneGen::Note::C5, 8}, {ToneGen::Note::E5, 4}, {ToneGen::Note::D5, 8}, {ToneGen::Note::C5, 8},
      {ToneGen::Note::B4, 4}, {ToneGen::Note::C5, 8}, {ToneGen::Note::D5, 4}, {ToneGen::Note::E5, 4},
      {ToneGen::Note::C5, 4}, {ToneGen::Note::A4, 4}, {ToneGen::Note::A4, 8}, {ToneGen::Note::A4, 4}, {ToneGen::Note::B4, 8}, {ToneGen::Note::C5, 8},

      {ToneGen::Note::D5, 4}, {ToneGen::Note::F5, 8}, {ToneGen::Note::A5, 4}, {ToneGen::Note::G5, 8}, {ToneGen::Note::F5, 8},
      {ToneGen::Note::E5, 4}, {ToneGen::Note::C5, 8}, {ToneGen::Note::E5, 4}, {ToneGen::Note::D5, 8}, {ToneGen::Note::C5, 8},
      {ToneGen::Note::B4, 4}, {ToneGen::Note::B4, 8}, {ToneGen::Note::C5, 8}, {ToneGen::Note::D5, 4}, {ToneGen::Note::E5, 4},
      {ToneGen::Note::C5, 4}, {ToneGen::Note::A4, 4}, {ToneGen::Note::A4, 4}, {ToneGen::Note::REST, 4},

      {ToneGen::Note::E5, 4}, {ToneGen::Note::B4, 8}, {ToneGen::Note::C5, 8}, {ToneGen::Note::D5, 4}, {ToneGen::Note::C5, 8}, {ToneGen::Note::B4, 8},
      {ToneGen::Note::A4, 4}, {ToneGen::Note::A4, 8}, {ToneGen::Note::C5, 8}, {ToneGen::Note::E5, 4}, {ToneGen::Note::D5, 8}, {ToneGen::Note::C5, 8},
      {ToneGen::Note::B4, 4}, {ToneGen::Note::C5, 8}, {ToneGen::Note::D5, 4}, {ToneGen::Note::E5, 4},
      {ToneGen::Note::C5, 4}, {ToneGen::Note::A4, 4}, {ToneGen::Note::A4, 8}, {ToneGen::Note::A4, 4}, {ToneGen::Note::B4, 8}, {ToneGen::Note::C5, 8},

      {ToneGen::Note::D5, 4}, {ToneGen::Note::F5, 8}, {ToneGen::Note::A5, 4}, {ToneGen::Note::G5, 8}, {ToneGen::Note::F5, 8},
      {ToneGen::Note::E5, 4}, {ToneGen::Note::C5, 8}, {ToneGen::Note::E5, 4}, {ToneGen::Note::D5, 8}, {ToneGen::Note::C5, 8},
      {ToneGen::Note::B4, 4}, {ToneGen::Note::B4, 8}, {ToneGen::Note::C5, 8}, {ToneGen::Note::D5, 4}, {ToneGen::Note::E5, 4},
      {ToneGen::Note::C5, 4}, {ToneGen::Note::A4, 4}, {ToneGen::Note::A4, 4}, {ToneGen::Note::REST, 4},


      {ToneGen::Note::E5, 2}, {ToneGen::Note::C5, 2},
      {ToneGen::Note::D5, 2}, {ToneGen::Note::B4, 2},
      {ToneGen::Note::C5, 2}, {ToneGen::Note::A4, 2},
      {ToneGen::Note::GS4, 2}, {ToneGen::Note::B4, 4}, {ToneGen::Note::REST, 8},
      {ToneGen::Note::E5, 2}, {ToneGen::Note::C5, 2},
      {ToneGen::Note::D5, 2}, {ToneGen::Note::B4, 2},
      {ToneGen::Note::C5, 4}, {ToneGen::Note::E5, 4}, {ToneGen::Note::A5, 2},
      {ToneGen::Note::GS5, 2}
    };

    while (true) {
        for (const auto [note, duration] : melody) {
            // Note
            gen.setFreq(static_cast<float>(note));
            gen.play();
            std::this_thread::sleep_for(std::chrono::milliseconds(1000 / duration));
            // Pause
            gen.setFreq(0);
            gen.play();
            std::this_thread::sleep_for(std::chrono::milliseconds(100 / duration));
        }
    }
}
